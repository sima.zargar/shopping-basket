import React from 'react';
import { connect } from 'react-redux';
import Product from '../Product/Product';

const Products = (props) => {
	return props.products.map((product) => {
		return (
			<Product
				key={product.id}
				id={product.id}
				name={product.name}
				price={product.price}
				stock={product.stock}
			/>
		)
	});
}

const mapStateToProps = state => {
	return {
		products: state.products
	}
};

export default connect(mapStateToProps)(Products);
