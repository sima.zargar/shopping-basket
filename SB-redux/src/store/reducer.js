const initState = {
	basketItems: [],
	products: [
  	  	{
	  	    id: 1,
	  	    name: "Ear Head Phone",
		    price: 10,
		    stock: 3
  	  	},
  	  	{
	  	    id: 2,
	  	    name: "Black Shoes",
		    price: 20,
		    stock: 0
  	  	},
  	  	{
	  	    id: 3,
	  	    name: "Petter Rabbit Movie",
		    price: 5,
		    stock: 6
  	  	}
	],
	totalPrice: 0
}

const reducer = (state = initState, action) => {
	const id = action.id;
	const basketItems = [...state.basketItems];
	const basketItem = basketItems.find((item) => item.id === id);
	const restOfBasketItems = basketItems.filter((item) => item.id !== id);
	let iBasketItem = {};

	const products = [...state.products];
	const restOfProducts = products.filter((item) => item.id !== id);
	const product = products.find((item) => item.id === id);
	let iProduct = {...product};

	switch (action.type) {
		case 'ADD_TO_BASKET':
			if (product.stock > 0) {
				if (basketItem === undefined) {
					
					iBasketItem = {...product};
					iBasketItem.quantity = 1;
				}
				else {
					iBasketItem = {...basketItem};
					iBasketItem.quantity += 1;
				}
				iProduct.stock -= 1;
				return {
					...state,
					basketItems: restOfBasketItems.concat(iBasketItem),
					products: restOfProducts.concat(iProduct),
					totalPrice: state.totalPrice + product.price
				}
			}
		break;
		case 'REDUCE_BASKET_ITEM':
			
			if (basketItem.quantity > 1) {
				iBasketItem = {...basketItem};
				iBasketItem.quantity -= 1;
				
				iProduct.stock += 1;

				return {
					...state,
					basketItems: restOfBasketItems.concat(iBasketItem),
					products: restOfProducts.concat(iProduct),
					totalPrice: state.totalPrice - basketItem.price
				}
			}
		break;
		case 'DELETE_BASKET_ITEM':
			
			iProduct.stock += basketItem.quantity;
			
			return {
				...state,
				basketItems: restOfBasketItems,
				products: restOfProducts.concat(iProduct),
				totalPrice: state.totalPrice - (basketItem.price * basketItem.quantity)
			}
		break;
	}
	return state;
}

export default reducer;



