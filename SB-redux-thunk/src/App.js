import React, { Component } from 'react';
import './App.css';
import Products from './component/Products/Products';
import Basket from './component/Basket/Basket';

class App extends Component {
	render() {
		return (
	      	<div className="App">
	        	<Products />
	    	    <Basket />
	      	</div>
	    );
  	}
}

export default App;
