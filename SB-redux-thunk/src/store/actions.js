export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const REDUCE_BASKET_ITEM = 'REDUCE_BASKET_ITEM';
export const DELETE_BASKET_ITEM = 'DELETE_BASKET_ITEM';

export const addToBasket = (id) => {
	return {
		type: ADD_TO_BASKET,
		id: id
	};
};

export const getBasketItemFromDB = (id) => {
	return {
		type: REDUCE_BASKET_ITEM,
		id: id
	};
}

export const reduceBasketItem = (id) => {
	return dispatch => {
		setTimeout( () => {
			dispatch(getBasketItemFromDB(id));
		}, 2000);
	}
};

export const deleteBasketItem = (id) => {
	return {
		type: DELETE_BASKET_ITEM,
		id: id
	};
};

//{type: 'ADD_TO_BASKET', id: id}