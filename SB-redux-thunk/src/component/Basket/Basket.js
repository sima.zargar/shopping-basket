import React from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions.js';
import './style.css'

const Basket = (props) => {
	const basketItems = props.basketItems.map((basketItem) => {
		return (
			<div 
				className="BasketItem" 
				key={basketItem.id}
			>
				Product Name: {basketItem.name}
				<br />
				Product Price: {basketItem.price}
				<br />
				Quantity: {basketItem.quantity}
				<br />
				<button onClick={() => props.reduceBasketItemHandler(basketItem.id)}>Reduce</button>
				&nbsp;
				<button onClick={() => props.deleteBasketItemHandler(basketItem.id)}>Delete</button>
			</div>
		)
  	})

  	return (
		<div className="Basket">
			Basket
			<br />
			Total Price: {props.totalPrice}
			{basketItems}
		</div>
  	)  
};

const mapStateToProps = state => {
	return {
		totalPrice: state.totalPrice,
		basketItems: state.basketItems
	}
};

const mapDispatchToProps = dispatch => {
	return {
		reduceBasketItemHandler: (id) => dispatch(actionTypes.reduceBasketItem(id)),
		deleteBasketItemHandler: (id) => dispatch(actionTypes.deleteBasketItem(id))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Basket);


