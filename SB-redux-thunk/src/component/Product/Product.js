import React from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/actions.js';
import './style.css';

const Product = (props) => {

	return (
		<div 
		 	className="Product"
		 	key={props.id}
		>
			Product Name: {props.name}
			<br />
			Product Price: {props.price}
			<br />
			Items left in stock: {props.stock}
			<br />
			<button 
				//onClick={props.addToBasketHandler.bind(this, props.id)}
				onClick={() => props.addToBasketHandler(props.id)}
				disabled={(props.stock < 1) ? 'disabled' : ''}
			>
				Add to Basket
			</button>
		</div>
	)
};

const mapDispatchToProps = dispatch => {
	return {
		addToBasketHandler: (id) => dispatch(actionTypes.addToBasket(id))
	};
};

export default connect(null, mapDispatchToProps)(Product);

