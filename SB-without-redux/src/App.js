import React, { Component } from 'react';
import './App.css';
import Products from './component/Products/Products';
import Basket from './component/Basket/Basket';

class App extends Component {

 	state = {
		products: [
	  	  	{
		  	    id: 1,
		  	    name: "Ear Head Phone",
			    price: 10,
			    stock: 3
	  	  	},
	  	  	{
		  	    id: 2,
		  	    name: "Black Shoes",
			    price: 20,
			    stock: 0
	  	  	},
	  	  	{
		  	    id: 3,
		  	    name: "Petter Rabbit Movie",
			    price: 5,
			    stock: 6
	  	  	}
		],
		totalPrice: 0,
		basketItems: []
    };

	addToBasketHandler = (id) => {
  		const basketItems = [...this.state.basketItems];
		const basketItem = basketItems.find((item) => item.id === id);
		const restOfBasketItems = basketItems.filter((item) => item.id !== id);
		let iBasketItem = {};

		const products = [...this.state.products];
  		const restOfProducts = products.filter((item) => item.id !== id);
  		const product = products.find((item) => item.id === id);
  		const iProduct = {...product};

		if (iProduct.stock > 0) {
			if (basketItem === undefined) {
				iBasketItem = {...product}; // golder line
				iBasketItem.quantity = 1;
			}
			else {
				iBasketItem = {...basketItem}; // golder line
				iBasketItem.quantity += 1;
			}
			iProduct.stock -= 1;
			this.setState({basketItems: restOfBasketItems.concat(iBasketItem)});
			this.setState({products: restOfProducts.concat(iProduct)});
			this.setState({totalPrice: this.state.totalPrice + product.price});
		}
  	}

	reduceBasketItemHandler = (id) => {
		const basketItems = [...this.state.basketItems];
		const basketItem = basketItems.find((item) => item.id === id);
		
		if (basketItem.quantity > 1) {
			const iBasketItem = {...basketItem}; // golder line
			const restOfBasketItems = basketItems.filter((item) => item.id !== id);

			const products = [...this.state.products];
			const restOfProducts = products.filter((item) => item.id !== id);
			const product = products.find((item) => item.id === id);
			const iProduct = {...product};
			
			iBasketItem.quantity -= 1;
			iProduct.stock += 1;
		
			this.setState({basketItems: restOfBasketItems.concat(iBasketItem)});
			this.setState({products: restOfProducts.concat(iProduct)});
			this.setState({totalPrice: this.state.totalPrice - basketItem.price});
		}
		
	}

	deleteBasketItemHandler = (id) => {
		const basketItems = [...this.state.basketItems];
		const basketItem = basketItems.find((item) => item.id === id);
		const restOfBasketItems = basketItems.filter((item) => item.id !== id);
		
		const products = [...this.state.products];
		const product = products.find((item) => item.id === id);
		const iProduct = {...product};
		const restOfProducts = products.filter((item) => item.id !== id);
		
		iProduct.stock += basketItem.quantity;
		
		this.setState({totalPrice: this.state.totalPrice - (basketItem.price*basketItem.quantity)});
		this.setState({products: restOfProducts.concat(iProduct)});
		this.setState({basketItems: restOfBasketItems});
	}

	render() {
		let basket = null;
		if (this.state.basketItems) {
			basket = (
				<Basket
					totalPrice={this.state.totalPrice}
					basketItems={this.state.basketItems}
					deleteBasketItemHandler={this.deleteBasketItemHandler}
					reduceBasketItemHandler={this.reduceBasketItemHandler}
	    		/>
		  	);
		}

		return (
	      	<div className="App">
	        	<Products 
	        		products={this.state.products}
		        	addToBasketHandler={this.addToBasketHandler}
		        />
	    	    {basket}
	      	</div>
	    );
  	}
}

export default App;
