import React from 'react';
import Product from '../Product/Product';

const Products = (props) => {
	return props.products.map((product) => {
		return (
			<Product
				key={product.id}
				id={product.id}
				name={product.name}
				price={product.price}
				stock={product.stock}
				addToBasketHandler={props.addToBasketHandler}
			/>
		)
	});
}

export default Products;