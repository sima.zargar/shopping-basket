import React from 'react';
import './style.css'

const Basket = (props) => {
	const basketItems = props.basketItems.map((basketItem) => {
		return (
			<div 
				className="BasketItem" 
				key={basketItem.id}
			>
				Product Name: {basketItem.name}
				<br />
				Product Price: {basketItem.price}
				<br />
				Quantity: {basketItem.quantity}
				<br />
				<button onClick={props.reduceBasketItemHandler.bind(this, basketItem.id)}>Reduce</button>
				&nbsp;
				<button onClick={props.deleteBasketItemHandler.bind(this, basketItem.id)}>Delete</button>
			</div>
		)
  	})

  	return (
		<div className="Basket">
			Basket
			<br />
			Total Price: {props.totalPrice}
			{basketItems}
		</div>
  	)  
};

export default Basket;


