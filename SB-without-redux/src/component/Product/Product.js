import React from 'react';
import './style.css';

const Product = (props) => {
	return (
		<div 
		 	className="Product"
		 	key={props.id}
		>
			Product Name: {props.name}
			<br />
			Product Price: {props.price}
			<br />
			Items left in stock: {props.stock}
			<br />
			<button 
				onClick={props.addToBasketHandler.bind(this, props.id)}
				disabled={(props.stock < 1) ? 'disabled' : ''}
			>
				Add to Basket
			</button>
		</div>
	)
};

export default Product;